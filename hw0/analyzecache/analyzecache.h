#ifndef H_ANALYZE_CACHE
#define H_ANALYZE_CACHE

// Your header code goes here, between #define and #endif
void cacheline();
void cachesize();
int cachemiss();
long long clock_time();
#endif

