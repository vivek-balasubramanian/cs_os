#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include<math.h>
#include "analyzecache.h"


int main(int argc, char *argv[])
{
	cacheline();
	cachesize();
	cachemiss();
	return 0;
}

void cacheline()
{

    //----------------------------------------------------------------------------------
    //Variable Declarations
    long int s,j,i,k;
    const int num_steps = 11;
    const int size_data = 1024;                 //KB
    long int length= 10*size_data*size_data;        //number of integers in array
    const int times=1;                              //number of times to repeat to average
    double time_data[times],sum=0,time_collect[num_steps];
    int tmp=0;                                  //temp variable
	double timeTaken;                           // record time
	clock_t start;                              // clock variable
	//----------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------
    // Fill out "jump" steps
	unsigned int steps[num_steps];
	for(i=0;i<num_steps;i++)
	    steps[i]=power(2,i);
    //----------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------
	// Create the huge data - need to keep it greater than page size (4MB)
    int *arr = malloc(length*sizeof(int)); //40MB

    //Avoid Cold Miss
    for (i = 0; i <length; i++)
        arr[i]=1;
    //----------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------
    // Open file to write timing data
    FILE *f = fopen("cacheline_timing.data","w");

    //----------------------------------------------------------------------------------
    // Vroom vroom ! Go !
    for (s=0;s<num_steps;s++)
    {
        k=steps[s];
		for(j=0;j<times;j++)
		{
	        start = clock_time();
	        for (i = 0; i <length; i++)
	        {
	            tmp+=arr[(i*k)%length];
	        }
        	timeTaken = (double)(clock_time() - start);
			time_data[j]=timeTaken;
		}
		sum=0;
		for(i=0;i<times;i++)
			sum+=time_data[i];
	    time_collect[s]=(double)sum/times;
        //printf("Block size : %lu bytes,", k*sizeof(int));
		//printf(" Time : %.8f ns\n",(double)sum/times);
		// Kbytes, Nanoseconds
		fprintf(f,"Bytes : %lu, Time : %.8f ns\n",k*sizeof(int),(double)sum/times);
    }
	//----------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------
    // cleaning up
    fclose(f);
	free(arr);

	//----------------------------------------------------------------------------------
	// Print Cacheline Size
    double max = 0;
    int max_i_1 = 0;
    for (i=0;i<num_steps-1;i++)
    {
        //printf("Change from %d to %d : %.8f \n",steps[i]*4,steps[i+1]*4,(time_collect[i+1]-time_collect[i])/time_collect[i]);
        if ((time_collect[i+1]-time_collect[i]) > 10000000 )
        {
            max_i_1 = steps[i]*4;
            break;
        }
    }
    printf("Cacheline Size : %d KB\n",max_i_1);
}

void cachesize()
{
	//----------------------------------------------------------------------------------
	// Variable Declarations
	long int i,j,tr;
	clock_t start;
	double time;

	const int num_sizes = 11;       // number of different array sizes
	int times=100;                   //number of times to repeat to average
	long int reps=8192*1024*10;          // number of repititive accesses to the array
	int jump=16;                    // cacheline size
	int size=0;                     // variable which takes mfact values

    int mfact[num_sizes];
	for(i=0;i<num_sizes;i++)
	{
	    mfact[i]=power(2,i);
	    //printf("%d\n",steps[i]);
	}

	double time_data[num_sizes];    // store timing data

	for (tr=0;tr<times;tr++)
	{
		for (i=0;i < num_sizes; i++)
		{
		    //----------------------------------------------------------------------------------
		    // Declare array
			size=mfact[i];
			int *arr = malloc(size*1024*sizeof(int));
			int length = size*1024 - 1;
			//----------------------------------------------------------------------------------

			//----------------------------------------------------------------------------------
			//Avoid Cold miss
		    for (j = 0; j < size*1024; j++)
	            arr[i]+=1;
	        //----------------------------------------------------------------------------------

	        //----------------------------------------------------------------------------------
	        // Record time !
			start=clock_time();
			for(j=0;j < reps; j+=jump)
				arr[(j) & length]++;
			time = (double)(clock_time() - start);
			time_data[i] += time;
			//----------------------------------------------------------------------------------

			//----------------------------------------------------------------------------------
			free(arr);
		}
	}

	//----------------------------------------------------------------------------------
	//File write
	FILE *f = fopen("cachesize_timing.data","w");
	for (i=0;i<num_sizes;i++)
		fprintf(f,"Bytes : %lu, %.8f ns\n",mfact[i]*sizeof(int),(double)(time_data[i]/times));
	fclose(f);
    //----------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------
    // Print CacheSize
    double max = 0;
    int max_i = 0;
    for (i=0;i<num_sizes-1;i++)
    {
        if ((time_data[i+1]-time_data[i])/times>1000000 )
        {
            max_i = mfact[i]*4;
            break;
        }
    }
    printf("Cache Size : %d KB\n",max_i);
}

int cachemiss()
{
	const int cachesize=256;
	const int linesize=64;
	const int length = 32768;
	const int rand_times=100;
	srand(time(NULL));
	int *data=malloc(length*sizeof(int));
	double sum;
	clock_t start;
	double time,time_linear[length],time_strided[length/linesize], time_rand[rand_times];
	int i,j;

	// Avoid Cold Miss
	for (j=0;j<32768;j++)
	{
		data[j]=1;
	}

    /*
	//printf("\n Linear Accesses :- \n\n");
	//Linear accesses
	for(j=0;j<length;j++)
	{
		start=clock_time();
		data[j]++;
		time = (double)(clock_time() - start);
		time_linear[j]=time;
	}
	//for(j=0;j<32768;j++)
	//	printf("%.8f \n",time_linear[j]);

    //----------------------------------------------------------------------------------
	//File write
	FILE *f1 = fopen("cachemiss_linear.data","w");
	for (i=0;i<length;i++)
		fprintf(f1,"Time : %.8f ns\n",time_linear[i]);
	fclose(f1);
    //----------------------------------------------------------------------------------
    */

	//printf("\nStrided Accesses :- \n\n");
	//Strided accesses
	i=0;
	for (j=0;j<length;j+=linesize)
    {
        start=clock_time();
        data[j]++;
        time = (double)(clock_time() - start);
        time_strided[i++]=time;
    }
    //for (i=0;i<512;i++)
    //    printf("%.8f \n",time_stride[i]);

    //----------------------------------------------------------------------------------
    // Find Average
    for (i=0;i<length/linesize;i++)
        sum+=time_strided[i];
    printf("Cache Miss Penalty : %lf ns \n",sum/(length/linesize));
    //----------------------------------------------------------------------------------
	//File write
	FILE *f2 = fopen("cachemiss_strided.data","w");
	for (i=0;i<length/linesize;i++)
		fprintf(f2,"Time : %.8f ns\n",time_strided[i]);
	fclose(f2);
    //----------------------------------------------------------------------------------

    /*
	//printf("\n Random Accesses :- \n\n");
	//Random access
	for (j=0;j<rand_times;j++)
	{
		start=clock_time();
		data[rand()%length]++;
		time = (double)(clock_time() - start);
		time_rand[j]=time;
	}
	//for (j=0;j<rand_times;j++)
	//	printf("%.8f \n",time_rand[j]);

	//----------------------------------------------------------------------------------
	//File write
	FILE *f3 = fopen("cachemiss_random.data","w");
	for (i=0;i<rand_times;i++)
		fprintf(f3,"Time : %.8f ns\n",(double)time_rand[i]);
	fclose(f3);
    //----------------------------------------------------------------------------------
    */
}
int power(int base, int exp)
{
    if (exp==0)
        return 1;
    return base*power(base,exp-1);
}
long long clock_time() 
{
	struct timespec tp;
	clock_gettime(CLOCK_REALTIME, &tp);
	return (long long)(tp.tv_nsec + (long long)tp.tv_sec * 1000000000);
}
