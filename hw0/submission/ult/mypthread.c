#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include <string.h>
#include "mypthread.h"


//====================================================
// THREAD-RELATED GLOBAL INITIALIZATIONS
static int tindex = 1;

//To keep track of current thread
struct mypthread_t *cur_thread = NULL;
struct mypthread_t *main_thread = NULL;

//First time "main" run
int maincheck = 1;
//====================================================


//====================================================
// HEADS and TAILS

//----------------------------------------------------
//Idle Queue
struct Iqueue* Ihead = NULL;
struct Iqueue* Itail;
//----------------------------------------------------

//----------------------------------------------------
//Running Queue
struct Rqueue* Rhead = NULL;
struct Rqueue* Rtail;
//----------------------------------------------------

//----------------------------------------------------
//Terminated Queue
struct Tqueue* Thead = NULL;
struct Tqueue* Ttail;
//----------------------------------------------------

//====================================================



//====================================================
// QUEUE FUNCTION DEFINITIONS

//----------------------------------------------------
// Idle Queue
void addToIQ(struct mypthread_t *th)
{
    struct Iqueue *temp = NULL;
    temp = malloc(sizeof(struct Iqueue));
    temp->thread = th;
    temp->next = NULL;
    Itail = temp;

    if(Ihead==NULL)
        Ihead=temp;
    else
    {
        struct Iqueue* cur = Ihead;
        while(cur->next != NULL)
        {
            cur=cur->next;
        }
        cur->next = temp;
    }
}

struct mypthread_t* popIQ()
{
    struct Iqueue *temp = Itail;
    struct Iqueue *cur = Ihead;

    if(Ihead==NULL)
        return NULL;

    if(cur->next!=NULL)
    {
        while((cur->next->next!=NULL))
        {
            cur=cur->next;
        }
        cur->next=NULL;
        Itail=cur;
        return temp->thread;
    }
    else
    {
        Ihead=NULL;
        Itail=NULL;
        return temp->thread;
    }
}

struct mypthread_t* popIQ_4m_front()
{
    struct Iqueue *temp = Ihead;

    if (Ihead == NULL)
        return NULL;

    Ihead = Ihead->next;
    return temp->thread;
}

int searchIQ(struct mypthread_t* th)
{
    struct Iqueue* temp = Ihead;
    while(temp!=NULL)
    {
        if((temp->thread)->index == th->index)
            return 1;
    }
    return 0;
}
//----------------------------------------------------

//----------------------------------------------------
// Running Queue
void addToRQ(struct mypthread_t* th)
{
    struct Rqueue* temp = NULL;
    temp = malloc(sizeof(struct Rqueue));
    temp->thread = th;
    temp->next = NULL;
    Rtail = temp;

    if(Rhead==NULL)
        Rhead=temp;
    else
    {
        struct Rqueue* cur = Rhead;
        while(cur->next != NULL)
        {
            cur=cur->next;
        }
        cur->next = temp;
    }
}

struct mypthread_t* popRQ()
{
    struct Rqueue *temp = Rtail;
    struct Rqueue *cur = Rhead;

    if(Rhead==NULL)
        return NULL;

    if(cur->next!=NULL)
    {
        while((cur->next->next!=NULL))
        {
            cur=cur->next;
        }
        cur->next=NULL;
        Rtail=cur;
        return temp->thread;
    }
    else
    {
        Rhead=NULL;
        Rtail=NULL;
        return temp->thread;
    }
}

struct mypthread_t* popRQ_4m_front()
{
    struct Rqueue *temp = Rhead;

    if (Rhead == NULL)
        return NULL;

    Rhead = Rhead->next;
    return temp->thread;
}

int searchRQ(struct mypthread_t* th)
{
    struct Rqueue* temp = Rhead;
    while(temp!=NULL)
    {
        if((temp->thread)->index == th->index)
            return 1;
    }
    return 0;
}
//----------------------------------------------------

//----------------------------------------------------
// Terminated Queue
void addToTQ(struct mypthread_t* th)
{
    struct Tqueue* temp = NULL;
    temp = malloc(sizeof(struct Tqueue));
    temp->thread = th;
    temp->next = NULL;
    Ttail = temp;

    if(Thead==NULL)
        Thead=temp;
    else
    {
        struct Tqueue* cur = Thead;
        while(cur->next != NULL)
        {
            cur=cur->next;
        }
        cur->next = temp;
    }
}

int searchTQ(struct mypthread_t* th)
{
    struct Tqueue* temp = Thead;
    while(temp!=NULL)
    {
        if((temp->thread)->index == th->index)
            return 1;
        temp=temp->next;
    }
    return 0;
}
//----------------------------------------------------

//====================================================

//====================================================
// THREADING FUNCTION DEFINITIONS

//----------------------------------------------------
//Thread Creation
int mypthread_create(mypthread_t *thread, const mypthread_attr_t *attr,void *(*func) (void *), void *arg)
{

    if(main_thread==NULL)
    {
        main_thread = malloc (sizeof(struct mypthread_t));
        memset(main_thread, 0, sizeof(struct mypthread_t));
        main_thread->index = tindex++;

        main_thread->ctx = (ucontext_t *) malloc(sizeof(ucontext_t));
        getcontext(main_thread->ctx);

        cur_thread = main_thread;
    }

    thread = malloc(sizeof(struct mypthread_t));
    memset(thread,0,sizeof(struct mypthread_t));
    thread->index = tindex++;

    thread->ctx = (ucontext_t *) malloc(sizeof(ucontext_t));
    getcontext(thread->ctx);
    thread->ctx->uc_stack.ss_sp = malloc(8192);
    thread->ctx->uc_stack.ss_size = 8192;
    thread->ctx->uc_link = NULL;
    makecontext(thread->ctx,(void (*)(void))func,1,arg);

    addToIQ(thread);

    return 0;
}
//----------------------------------------------------

//----------------------------------------------------
//Thread Join
int mypthread_join(mypthread_t thread, void **retval)
{
    if(searchTQ(&thread))
    {
        return -1;
    }

    mypthread_t *next_thread = popIQ_4m_front();
    mypthread_t *running_thread = popRQ();

    if (running_thread == NULL)
        running_thread = main_thread;
    else
        addToIQ(running_thread);


    addToRQ(next_thread);

    cur_thread = next_thread;
    swapcontext(running_thread->ctx,next_thread->ctx);
}
//----------------------------------------------------

//----------------------------------------------------
// Thread Yield
int mypthread_yield(void)
{
    if(Ihead==NULL)
    {
        return -1;
    }

    mypthread_t *next_thread = popIQ_4m_front();
    mypthread_t *running_thread = popRQ();

    addToIQ(running_thread);
    addToRQ(next_thread);

    cur_thread = next_thread;

    swapcontext(running_thread->ctx,next_thread->ctx);

    return 0;
}
//----------------------------------------------------

//----------------------------------------------------
// Thread exit
void mypthread_exit(void *retval)
{
    mypthread_t *running_thread = popRQ();
    mypthread_t *next_thread = main_thread;

    addToTQ(running_thread);

    cur_thread = main_thread;
    swapcontext(running_thread->ctx,main_thread->ctx);

}
//----------------------------------------------------

//====================================================
