#ifndef H_MYPTHREAD
#define H_MYPTHREAD

//====================================================
// THREAD DETAILS


// Thread Structure
typedef struct mypthread_t{
int index;
int size;
ucontext_t *ctx;
}mypthread_t;

// Thread Attributes
typedef struct mypthread_attr_t{
	// Define any fields you might need inside here.
}mypthread_attr_t;
//====================================================


//====================================================
// QUEUE STRUCTURES

//Idle Queue Structure
struct Iqueue{
    struct mypthread_t *thread;
    struct Iqueue *next;
};


//Running Queue Structure
struct Rqueue{
    struct mypthread_t *thread;
    struct Rqueue *next;
};

//Terminated Queue
struct Tqueue{
    struct mypthread_t *thread;
    struct Tqueue *next;
};
//====================================================


//====================================================
// QUEUE FUNCTIONS

//Idle Queue
void addToIQ(struct mypthread_t *th);
struct mypthread_t* popIQ();
struct mypthread_t* popIQ_4m_front();
int searchIQ(struct mypthread_t* th);

//Running Queue
void addToRQ(struct mypthread_t* th);
struct mypthread_t* popRQ();
struct mypthread_t* popRQ_4m_front();
int searchRQ(struct mypthread_t* th);

//Terminated Queue
void addToTQ(struct mypthread_t* th);
int searchTQ(struct mypthread_t* th);

//====================================================

//====================================================
// CUSTOM THREADING FUNCTIONS
// Functions
int mypthread_create(mypthread_t *thread, const mypthread_attr_t *attr,
			void *(*start_routine) (void *), void *arg);

void mypthread_exit(void *retval);

int mypthread_yield(void);

int mypthread_join(mypthread_t thread, void **retval);
//====================================================

/* Don't touch anything after this line.
 *
 * This is included just to make the mtsort.c program compatible
 * with both your ULT implementation as well as the system pthreads
 * implementation. The key idea is that mutexes are essentially
 * useless in a cooperative implementation, but are necessary in
 * a preemptive implementation.
 */

typedef int mypthread_mutex_t;
typedef int mypthread_mutexattr_t;

static inline int mypthread_mutex_init(mypthread_mutex_t *mutex,
			const mypthread_mutexattr_t *attr) { return 0; }

static inline int mypthread_mutex_destroy(mypthread_mutex_t *mutex) { return 0; }

static inline int mypthread_mutex_lock(mypthread_mutex_t *mutex) { return 0; }

static inline int mypthread_mutex_trylock(mypthread_mutex_t *mutex) { return 0; }

static inline int mypthread_mutex_unlock(mypthread_mutex_t *mutex) { return 0; }

#endif
